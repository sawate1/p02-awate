//
//  F3HTileAppearanceProvider.m
//  NumberTileGame
//
//  Created by Shivani Awate on 2/5/17.
//  Copyright © 2017 Shivani Awate. All rights reserved.
//


#import "F3HTileAppearanceProvider.h"

@implementation F3HTileAppearanceProvider

- (UIColor *)tileColorForValue:(NSUInteger)value {
    switch (value) {
        case 2:
            return [UIColor colorWithRed:255./255. green:160./255. blue:122./255. alpha:1];
        case 4:
            return [UIColor colorWithRed:240./255. green:128./255. blue:128./255. alpha:1];
        case 8:
            return [UIColor colorWithRed:205./255. green:92./255. blue:92./129. alpha:1];
        case 16:
            return [UIColor colorWithRed:255./255. green:99./255. blue:71./255. alpha:1];
        case 32:
            return [UIColor colorWithRed:220./255. green:20./255. blue:60./255. alpha:1];
        case 64:
            return [UIColor colorWithRed:178./255. green:34./255. blue:34./255. alpha:1];
        case 128:
        case 256:
        case 512:
        case 1024:
        case 2048:
            return [UIColor colorWithRed:237./255. green:207./255. blue:114./255. alpha:1];
        default:
            return [UIColor whiteColor];
    }
}
- (UIColor *)numberColorForValue:(NSUInteger)value {
    switch (value) {
        case 2:
        case 4:
            return [UIColor colorWithRed:119./255. green:110./255. blue:101./255. alpha:1];
        default:
            return [UIColor whiteColor];
    }
}

- (UIFont *)fontForNumbers {
    return [UIFont fontWithName:@"GillSans-UltraBold" size:20];
   
}

@end
