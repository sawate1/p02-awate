//
//  F3HTileAppearanceProvider.h
//  NumberTileGame
//
//  Created by Shivani Awate on 2/5/17.
//  Copyright © 2017 Shivani Awate. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol F3HTileAppearanceProviderProtocol <NSObject>

- (UIColor *)tileColorForValue:(NSUInteger)value;
- (UIColor *)numberColorForValue:(NSUInteger)value;
- (UIFont *)fontForNumbers;

@end

@interface F3HTileAppearanceProvider : NSObject <F3HTileAppearanceProviderProtocol>

@end
