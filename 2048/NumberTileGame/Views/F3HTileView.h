//
//  F3HTileView.h
//  NumberTileGame
//
//  Created by Shivani Awate on 2/7/17.
//  Copyright © 2017 Shivani Awate. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol F3HTileAppearanceProviderProtocol;
@interface F3HTileView : UIView

@property (nonatomic) NSInteger tileValue;

@property (nonatomic, weak) id<F3HTileAppearanceProviderProtocol>delegate;

+ (instancetype)tileForPosition:(CGPoint)position
                     sideLength:(CGFloat)side
                          value:(NSUInteger)value
                   cornerRadius:(CGFloat)cornerRadius;

@end
