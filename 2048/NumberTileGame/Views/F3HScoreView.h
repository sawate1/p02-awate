//
//  F3HScoreView.h
//  NumberTileGame
//
//  Created by Shivani Awate on 2/8/17.
//  Copyright © 2017 Shivani Awate. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface F3HScoreView : UIView

@property (nonatomic) NSInteger score;

+ (instancetype)scoreViewWithCornerRadius:(CGFloat)radius
                          backgroundColor:(UIColor *)color
                                textColor:(UIColor *)textColor
                                 textFont:(UIFont *)textFont;

@end
