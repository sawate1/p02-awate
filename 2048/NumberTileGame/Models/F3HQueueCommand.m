//
//  F3HQueueCommand.m
//  NumberTileGame
//
//  Created by Shivani Awate on 2/8/17.
//  Copyright © 2017 Shivani Awate. All rights reserved.
//


#import "F3HQueueCommand.h"

@implementation F3HQueueCommand

+ (instancetype)commandWithDirection:(F3HMoveDirection)direction
                     completionBlock:(void(^)(BOOL))completion {
    F3HQueueCommand *command = [[self class] new];
    command.direction = direction;
    command.completion = completion;
    return command;
}

@end
