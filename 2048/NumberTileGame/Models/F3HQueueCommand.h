//
//  F3HQueueCommand.h
//  NumberTileGame
//
//
//  Created by Shivani Awate on 2/8/17.
//  Copyright © 2017 Shivani Awate. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "F3HGameModel.h"

@interface F3HQueueCommand : NSObject

@property (nonatomic) F3HMoveDirection direction;
@property (nonatomic, copy) void(^completion)(BOOL atLeastOneMove);

+ (instancetype)commandWithDirection:(F3HMoveDirection)direction completionBlock:(void(^)(BOOL))completion;

@end
