//
//  F3HTileModel.h
//  NumberTileGame
//
//  Created by Shivani Awate on 2/7/17.
//  Copyright © 2017 Shivani Awate. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface F3HTileModel : NSObject

@property (nonatomic) BOOL empty;
@property (nonatomic) NSUInteger value;
+ (instancetype)emptyTile;

@end
