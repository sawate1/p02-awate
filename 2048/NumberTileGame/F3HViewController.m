//
//  F3HViewController.m
//  NumberTileGame
//
//
//  Created by Shivani Awate on 2/5/17.
//  Copyright © 2017 Shivani Awate. All rights reserved.
//

#import "F3HViewController.h"

#import "F3HNumberTileGameViewController.h"

@interface F3HViewController ()
@end

@implementation F3HViewController

- (IBAction)playGameButtonTapped:(id)sender {
    F3HNumberTileGameViewController *c = [F3HNumberTileGameViewController numberTileGameWithDimension:4
                                                                                         winThreshold:2048
                                                                                      backgroundColor:[UIColor whiteColor]
                                                                                          scoreModule:YES
                                                                                       buttonControls:NO
                                                                                        swipeControls:YES];
    [self presentViewController:c animated:YES completion:nil];
}

@end
