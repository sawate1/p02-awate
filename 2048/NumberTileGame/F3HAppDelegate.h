//
//  F3HAppDelegate.h
//  NumberTileGame
//  2048Tests
//
//  Created by Shivani Awate on 2/5/17.
//  Copyright © 2017 Shivani Awate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface F3HAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
